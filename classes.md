User
- String name
- String phoneNumber
- String email
+ String getName()
+ void setName(string name)
+ String getPhoneNumber()
+ void setPhoneNumber(string phoneNumber)
+ String getEmail()
+ void setEmail(string email)

Account
- String name
- int balance
- int treshold
- List<History> history
+ String getName()
+ void setName(String name)
+ void addBalance(int amount)
+ int getTreshold()
+ void setTreshold(int treshold)
+ List<History> getHistory()

History
- int amount
- Date createdAt
- string shortDesc
+ int getAmount()
+ Date getCreatedAt()



/ = login home GET
/login = login POST
/auth = authenticate POST
/deauth = deuthenticate POST
/account = accounts list GET
/account/:name = account detail GET
/account/add_account = add account POST
/account/:name/add_balance = add balance POST
