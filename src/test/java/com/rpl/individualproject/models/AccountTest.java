package com.rpl.individualproject.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class AccountTest {
    private Account account1;

    @BeforeEach
    void setUp() {
        account1 = new Account("Bank Acc", 500000, "savings in A bank");
    }

    @Test
    void setNameTest() {
        String name = "nabila";
        account1.setName(name);
        assertThat(account1.getName().equals(name)).isTrue();
    }

    @Test
    void setShortDescTest() {
        String desc = "bank savings";
        account1.setShortDesc(desc);
        assertThat(account1.getShortDesc().equals(desc)).isTrue();
    }

    @Test
    void setThresholdTest() {
        int threshold = 354000;
        account1.setThreshold(threshold);
        assertThat(account1.getThreshold() == 354000).isTrue();
    }

    @Test
    void addBalanceTest() {
        account1.addBalance(500000, "got from parents", new Date());
        assertThat(account1.getBalance()==500000).isTrue();
        assertThat(account1.getHistory().size() == 1).isTrue();
    }
}
