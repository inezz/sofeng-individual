package com.rpl.individualproject.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class UserTest {
    private User user;
    private Account account1;
    private Account account2;

    @BeforeEach
    void setUp() {
        user = new User("inez z", "081208120812", "inez@mail.com", "admin");
        account1 = new Account("Bank Acc", 500000, "savings in A bank");
        account2 = new Account("Savings N", 300000, "bank B for emergency");
    }

    @Test
    void setNameTest() {
        String name = "nabila";
        user.setName(name);
        assertThat(user.getName().equals(name)).isTrue();
    }

    @Test
    void setPhoneNumberTest() {
        String phone = "081108110811";
        user.setPhoneNumber(phone);
        assertThat(user.getPhoneNumber().equals(phone)).isTrue();
    }

    @Test
    void setEmailTest() {
        String email = "as@mail.com";
        user.setEmail(email);
        assertThat(user.getEmail().equals(email)).isTrue();
    }

    @Test
    void addAccountTest() {
        user.addAccount(account1);
        user.addAccount(account2);
        List<Account> accounts = user.getAccounts();
        assertThat(accounts.contains(account1)).isTrue();
        assertThat(accounts.contains(account2)).isTrue();
    }

    @Test
    void whenNoArgsContructorIsCalled(){
        User test = new User();
        assertThat(test.getPassword()).isNull();
    }
}

