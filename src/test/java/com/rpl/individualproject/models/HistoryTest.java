package com.rpl.individualproject.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class HistoryTest {
    private History history;
    private Date date;

    @BeforeEach
    void setUp() {
        date = new Date();
        history = new History(500000, "from x", date);
    }

    @Test
    void setDescTest() {
        String descr = "pocket money from dad";
        history.setDescription(descr);
        assertThat(history.getDescription().equals(descr)).isTrue();
    }

    @Test
    void getAmountTest() {
        int amount = 500000;
        assertThat(history.getAmount()==amount).isTrue();
    }

    @Test
    void getCreatedAtTest() {
        assertThat(history.getCreatedAt().equals(date)).isTrue();
    }
}
