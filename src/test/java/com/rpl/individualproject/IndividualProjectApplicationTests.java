package com.rpl.individualproject;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.springframework.test.util.AssertionErrors.assertNotEquals;

@SpringBootTest
class IndividualProjectApplicationTests {

	@Test
	void contextLoads() {
		IndividualProjectApplication.main(new String[]{});
		assertNotEquals("Error", true, false);

	}

}
