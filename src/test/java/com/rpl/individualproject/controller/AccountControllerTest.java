package com.rpl.individualproject.controller;

import com.rpl.individualproject.models.Account;
import com.rpl.individualproject.models.History;
import com.rpl.individualproject.models.User;
import com.rpl.individualproject.repository.AccountRepository;
import com.rpl.individualproject.repository.HistoryRepository;
import com.rpl.individualproject.repository.UserRepository;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.servlet.http.Cookie;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("Account")
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private HistoryRepository historyRepository;

    private User user;
    private Cookie cookie;
    private Account account1;
    private Account account2;
    private History history;

    @BeforeEach
    public void setUpTest() {
        user = new User("inez", "0812", "inez@mail.com", "1212");
        cookie = new Cookie("user_email", user.getEmail());
        account1 = new Account("Account1", 50000, "account dummy");
        account2 = new Account("Account2", 500000, "account dummy 2");

        // public History addBalance(int amount, String description, Date createdAt)
    }

    @Test
    public void urlPageTest() throws Exception {
        when(userRepository.getUserByEmail(user.getEmail())).thenReturn(user);
        mockMvc.perform(get("/account/").cookie(cookie))
                .andExpect(status().isOk())
                .andExpect(view().name("account"));
    }
    @Test
    public void addBalanceTest() throws Exception {
        when(userRepository.getUserByEmail(user.getEmail())).thenReturn(user);
        mockMvc.perform(get("/account/add_balance/").cookie(cookie))
                .andExpect(status().isOk())
                .andExpect(view().name("updatebalance"));
    }

    @Test
    public void checkHistoryNoAccountTest() throws Exception {
        when(userRepository.getUserByEmail(user.getEmail())).thenReturn(user);
        mockMvc.perform(get("/account/0/").cookie(cookie))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/account/"));
        user.addAccount(account1);
        user.addAccount(account2);
        mockMvc.perform(get("/account/0/").cookie(cookie))
                .andExpect(status().isOk())
                .andExpect(view().name("historypage"));
    }

}
