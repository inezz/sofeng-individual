package com.rpl.individualproject.controller;

import com.rpl.individualproject.models.Account;
import com.rpl.individualproject.models.History;
import com.rpl.individualproject.models.User;
import com.rpl.individualproject.repository.AccountRepository;
import com.rpl.individualproject.repository.HistoryRepository;
import com.rpl.individualproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
@RequestMapping("/account/")
public class AccountController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private HistoryRepository historyRepository;

    @GetMapping("/")
    public String getAccountPage(@CookieValue(value = "user_email", defaultValue = "") String userEmail, Model model) {
        if (userEmail.isEmpty()) return "redirect:/";
        User user = userRepository.getUserByEmail(userEmail);
        model.addAttribute("user", user);
        return "account";
    }


    @PostMapping("/add_account")
    public String postAddAccount(
            @CookieValue(value = "user_email", defaultValue = "") String userEmail,
            @RequestBody MultiValueMap<String, String> formData, Model model) {
        if (userEmail.isEmpty()) return "redirect:/";
        String name = formData.getFirst("accountName");
        String threshold = formData.getFirst("threshold");
        String shortDesc = formData.getFirst("shortDesc");
        User user = userRepository.getUserByEmail(userEmail);
        Account account = new Account(name, Integer.parseInt(threshold), shortDesc);
        user.addAccount(account);
        user = userRepository.save(user);
        account = accountRepository.save(account);
        return "redirect:/account/";
    }

    @GetMapping("/add_balance")
    public String getAddBalancePage(@CookieValue(value = "user_email", defaultValue = "") String userEmail, Model model) {
        if (userEmail.isEmpty()) return "redirect:/";
        User users = userRepository.getUserByEmail(userEmail);
        model.addAttribute("accounts", users.getAccounts());
        return "updatebalance";
    }

    @PostMapping("/add_balance")
    public String postAddBalance(
            @CookieValue(value = "user_email", defaultValue = "") String userEmail,
            @RequestBody MultiValueMap<String, String> formData, Model model) {
        if (userEmail.isEmpty()) return "redirect:/";
        int amount = Integer.parseInt(formData.getFirst("amount"));
        String balanceType = formData.getFirst("balanceType");
        String shortDesc = formData.getFirst("shortDescription");
        int id = Integer.parseInt(formData.getFirst("accountId"));
        Account account = accountRepository.getOneById(id);
        amount *= balanceType.equals("income") ? 1 : -1;
        History history = account.addBalance(amount, shortDesc, new Date());
        historyRepository.save(history);
        return "redirect:/account/";
    }

    @GetMapping("/{accountId}")
    public String getHistoryPage(@CookieValue(value = "user_email", defaultValue = "") String userEmail, @PathVariable(required = true) String accountId, Model model) {
        if (userEmail.isEmpty()) return "redirect:/";
        User user = userRepository.getUserByEmail(userEmail);
        for (Account account : user.getAccounts()) {
            if (account.getId() == Integer.parseInt(accountId)) {

                model.addAttribute("account", account);
                return "historypage";
            }
        }
        return "redirect:/account/";
    }
}
