package com.rpl.individualproject.controller;

import com.rpl.individualproject.models.User;
import com.rpl.individualproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/")
public class LoginController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/")
    public String getLoginPage(@CookieValue(value = "user_email", defaultValue = "") String userEmail, Model model) {
        if (!userEmail.isEmpty()) return "redirect:/account/";
        return "login";
    }
    
    @PostMapping("/login")
    public String postLogin(
        @CookieValue(value = "user_email", defaultValue = "") String userEmail, 
        @RequestBody MultiValueMap<String, String> formData,  HttpServletResponse response) {
        if (userEmail.isEmpty()) {
            String email = formData.getFirst("email");
            String password = formData.getFirst("password");
            User user = userRepository.getUserByEmail(email);
            if (user!=null && user.getPassword().equals(password)) {
                Cookie cookie = new Cookie("user_email", user.getEmail());
                cookie.setMaxAge(3600 * 24);
                cookie.setHttpOnly(true);
                cookie.setPath("/");
                response.addCookie(cookie);
                return "redirect:/account/";
            }
        }
        return "redirect:/";
    }
        
    @GetMapping("/logout")
    public String getLogout(Model model, HttpServletResponse response) {
        Cookie cookie = new Cookie("user_email", null);
        cookie.setMaxAge(0);
        cookie.setHttpOnly(true);
        cookie.setPath("/");
        response.addCookie(cookie);
        return "redirect:/";
    }
    
    @GetMapping("/signup")
    public String getSignupPage(@CookieValue(value = "user_email", defaultValue = "") String userEmail, Model model) {
        if (!userEmail.isEmpty()) return "redirect:/account/";
        return "signup";
    }
    
    @PostMapping("/signup")
    public String postSignup(
        @CookieValue(value = "user_email", defaultValue = "") String userEmail,
        @RequestBody MultiValueMap<String, String> formData, Model model) {
        if (!userEmail.isEmpty()) return "redirect:/account/";
        // TODO: form validation
        String name = formData.getFirst("name");
        String phone = formData.getFirst("phonenbr");
        String email = formData.getFirst("email");
        String password = formData.getFirst("password");

        User user = new User( name,  phone,  email,  password);
        userRepository.save(user);
        return "redirect:/";
    }
}
