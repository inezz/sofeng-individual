package com.rpl.individualproject.repository;

import com.rpl.individualproject.models.Account;
import com.rpl.individualproject.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account getOneByName(String accountName);
    List<Account> findAllByUser(User user);
    Account getOneById(Integer id);

}