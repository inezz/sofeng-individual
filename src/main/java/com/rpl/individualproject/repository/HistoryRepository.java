package com.rpl.individualproject.repository;

import com.rpl.individualproject.models.Account;
import com.rpl.individualproject.models.History;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface HistoryRepository extends JpaRepository<History, Integer> {
}