package com.rpl.individualproject.models;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="accounts")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @NotNull
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @NotNull
    @Column(name = "balance", nullable = false)
    private int balance;

    @NotNull
    @Column(name = "shortDesc", nullable = false)
    private String shortDesc;

    @NotNull
    @Column(name = "threshold", nullable = false)
    private int threshold;

    @OneToMany(mappedBy="account")
    private List<History> history;


    @ManyToOne
    User user;

    public Account(){

    }

    public Account(String name, int threshold, String shortDesc) {
        this.name = name;
        this.balance = 0;
        this.threshold = threshold;
        this.shortDesc = shortDesc;
        this.history = new ArrayList<History>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBalance() {
        return balance;
    }

    public History addBalance(int amount, String description, Date createdAt) {
        balance += amount;
        History history = new History(amount, description, createdAt);
        this.history.add(history);
        history.setAccount(this);
        return history;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public List<History> getHistory() {
        return history;
    }

    public int getId() {
        return id;
    }
}
